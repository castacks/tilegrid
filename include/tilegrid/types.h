#ifndef TYPES_H_DBHPSQXJ
#define TYPES_H_DBHPSQXJ

#include <cstdint>
#include <Eigen/Core>

namespace tilegrid
{

// a compact bitwise representation of ijk coordinates
using hash_ix_t = std::uint64_t;
using HashIxVector = Eigen::Matrix<hash_ix_t, 1, Eigen::Dynamic>;

/* linear index type *************************/
// changing mix_t to match vdb::Index
using mix_t = std::uint32_t;
using mix64_t = std::uint64_t;

// vectors of linear index types
template<int D> using VecDMIx = Eigen::Matrix<mix_t, 1, D>;

using Vec2MIx = VecDMIx<2>;
using Vec3MIx = VecDMIx<3>;
using Vec4MIx = VecDMIx<4>;
using VecXMIx = Eigen::Matrix<mix_t, 1, Eigen::Dynamic>;

/* grid indexing types ***********************/
using ugix_t = std::uint32_t;
using gix_t = std::int32_t;

// vectors of grid indexing types
// indexes grids (i, j, [k]), cf. vdb::Coord
template<int D> using VecDIx = Eigen::Matrix<gix_t, 1, D>;

using Vec2Ix = VecDIx<2>;
using Vec3Ix = VecDIx<3>;

// collection of grid indexing types
template<int D> using MatDIx = Eigen::Matrix<gix_t, Eigen::Dynamic, D, Eigen::RowMajor>;

using Mat2Ix = MatDIx<2>;
using Mat3Ix = MatDIx<3>;

} /* tilegrid */

#endif /* end of include guard: TYPES_H_DBHPSQXJ */
