#ifndef FIXEDROOTGRID_H_KCNDPWVZ
#define FIXEDROOTGRID_H_KCNDPWVZ

#include <cstdint>
#include <memory>
#include <sstream>
#include <iostream>
#include <bitset>
#include <unordered_map>
#include <map>
#include <set>
#include <tuple>

#include <ros/console.h>

#include "tilegrid/types.h"
#include "tilegrid/node_masks.h"
#include "tilegrid/box.h"

// TODO what about attachign tile-level
// metadata to each cell?
// e.g. max, min? or do it with parallel grid?

namespace tilegrid
{

template<typename ChildGridT, mix_t Log2DimT>
class FixedRootGrid2 {
public:
  using ChildType = ChildGridT;

  static constexpr mix_t Log2Dim = Log2DimT;
  static constexpr mix_t Total = Log2Dim + ChildType::Log2Dim;
  static constexpr mix_t Dim = 1 << Log2Dim;
  static constexpr mix_t TotalDim = 1 << Total;
  static constexpr mix_t NumGrids = 1 << (2*Log2Dim);
  static constexpr mix64_t NumCells = uint64_t(1) << (2*Total);

  using CellType = typename ChildType::CellType;

  using ArrayType = std::array<ChildType*, NumGrids>;

  using VecIx = Vec2Ix;
  using VecMIx = Vec2MIx;
  using MatIx = Mat2Ix;
  using Ptr = std::shared_ptr<FixedRootGrid2>;
  using ConstPtr = std::shared_ptr<const FixedRootGrid2>;
  using BitMask = NodeMask<Log2Dim, 2>;
  using BitMaskOnIterator = typename BitMask::OnIterator;
  //using TotalBitMask = NodeMask<Total, 2>;
  using BoxType = typename ChildType::BoxType;

public:

  FixedRootGrid2() :
      background_()
  { }

  FixedRootGrid2(const CellType& background) :
      background_(background)
  { }

  virtual ~FixedRootGrid2() {
    this->clear();
  }

  void clear() {
    for (BitMaskOnIterator on_itr(child_mask_.beginOn());
         on_itr.test();
         ++on_itr) {
      mix_t mix = on_itr.offset();
      if (child_grid_[mix] != nullptr) {
        delete child_grid_[mix];
        child_grid_[mix] = nullptr;
      }
    }
    child_mask_.setOff();
  }

  FixedRootGrid2(const FixedRootGrid2& other) = delete;
  FixedRootGrid2& operator=(const FixedRootGrid2& other) = delete;

  bool empty() const { return child_mask_.isOff(); }

  /**
   * offset into child grid from global grid ix
   */
  mix_t grid_to_offset(const VecIx& gix) const {
    // e.g. if Tree(3, 3)
    // - first zero out all bits corresponding to things outside this grid,
    //   i.e. keep only lower 5 bits
    // - then for x coord keep bits 5,4,3 by shifting, and leave in orig.
    //   position
    // - then for y coord keep bits 5,4,3 and shift to right
    // so then lower 6 bits of offset look like [x5 x4 x3][y5 y4 y3],
    // higher bits are 0
    // recall that the lower 3 bits are used to index inside leaf

    // TODO this doesn't do any bound checking,
    // and it seems like a valid offset will always happen.
    // probably things will wrap around.
    // but semantically, not sure what we want. should I assign
    // origin and subtract it from gix to ensure (0, TotalDim-1) range
    // for gix? Or should this happen in TileGrid with the origin member?

    return (((gix[0] & (TotalDim-1u)) >> ChildType::Log2Dim) << Log2Dim)
          + ((gix[1] & (TotalDim-1u)) >> ChildType::Log2Dim);
  }

  /**
   * given offset of child in child grid, get
   * local grid ix
   * I think
   */
  VecIx offset_to_local_grid(mix_t mix) const {
    mix &= ((1<<2*Log2Dim)-1);
    return VecIx(mix >> Log2Dim, mix & ((1<<Log2Dim)-1));
  }

  const CellType& get_value(const VecIx& gix) const {
    mix_t mix = this->grid_to_offset(gix);
    return (child_mask_.isOff(mix)) ?
            background_ :
            child_grid_[mix]->get_value(gix);
  }

  // ptr, to signal non-existent voxel case
  CellType* get_value_ptr(const VecIx& gix) {
    mix_t mix = this->grid_to_offset(gix);
    return (child_mask_.isOff(mix)) ?
            nullptr :
            child_grid_[mix]->get_value_ptr(gix);
  }

  void set_value(const VecIx& gix, const CellType& val) {
    mix_t mix = this->grid_to_offset(gix);
    std::cerr << "gix = " << gix << ", mix = " << mix << std::endl;
    if (child_mask_.isOn(mix)) {
      child_grid_[mix]->set_value(gix, val);
    } else {
      this->insert_child_with_value(mix, gix, val);
    }
  }

  // doesn't make voxels active, just changes value
  void fill(const CellType& val) {
    if (this->empty()) { return; }
    for (ChildOnIterator itr = this->begin_child_on(); !itr.done(); ++itr) {
      itr->fill(val);
    }
  }

  void set_inactive() {
    if (this->empty()) { return; }
    for (ChildOnIterator itr = this->begin_child_on(); !itr.done(); ++itr) {
      itr->set_inactive();
    }
    child_mask_.setOff();
  }

  /*
   * May return nullptr. Use with caution!
   */
  ChildType* get_child(const VecIx& gix) const {
    mix_t mix = this->grid_to_offset(gix);
    return child_grid_[mix];
  }

  /**
   * Only use if you know you need it
   */
  ChildType* insert_child_with_value(mix_t mix, const VecIx& gix, const CellType& val) {
    ChildType *child(new ChildType(gix, background_, false));
    child->set_value(gix, val);
    child_grid_[mix] = child;
    child_mask_.setOn(mix);
    return child;
  }

  bool probe(const VecIx& gix) const {
    mix_t mix = this->grid_to_offset(gix);
    if (child_mask_.isOn(mix)) {
      return child_grid_[mix]->probe(gix);
    }
    return false;
  }

  mix_t active_cell_count() const {
    mix_t count(0);
    for (ChildOnIterator itr = this->begin_child_on(); !itr.done(); ++itr) {
      count += itr->active_cell_count();
    }
    return count;
  }

  size_t allocated_bytes() const {
    // TODO cache
    size_t sz(sizeof(*this));
    for (ChildOnIterator itr = this->begin_child_on(); !itr.done(); ++itr) {
      sz += itr->allocated_bytes();
    }
    return sz;
  }

  VecIx min_grid_ix() const {
    if (this->empty()) { return VecIx::Zero(); }
    VecIx min_ix = VecIx::Constant(std::numeric_limits<gix_t>::max());
    for (ChildOnIterator itr = this->begin_child_on(); !itr.done(); ++itr) {
      VecIx mi(itr->min_grid_ix());
      min_ix = min_ix.array().min(mi.array());
    }
    return min_ix;
  }

  VecIx max_grid_ix() const {
    if (this->empty()) { return VecIx::Zero(); }
    VecIx max_ix = VecIx::Constant(std::numeric_limits<gix_t>::min());
    for (ChildOnIterator itr = this->begin_child_on(); !itr.done(); ++itr) {
      VecIx Mi(itr->max_grid_ix());
      max_ix = max_ix.array().max(Mi.array());
    }
    return max_ix;
  }

  BoxType bbox() const {
    if (this->empty()) { return BoxType(); }
    VecIx min_ix = VecIx::Constant(std::numeric_limits<gix_t>::max());
    VecIx max_ix = VecIx::Constant(std::numeric_limits<gix_t>::min());
    for (ChildOnIterator itr = this->begin_child_on(); !itr.done(); ++itr) {
      VecIx mi(itr->min_grid_ix());
      VecIx Mi(itr->max_grid_ix());
      min_ix = min_ix.array().min(mi.array());
      max_ix = max_ix.array().max(Mi.array());
    }
    return BoxType(min_ix, max_ix);
  }

  const CellType& background() { return background_; }

public:
  struct ChildOnIterator {
    ChildOnIterator() { }
    ChildOnIterator(BitMaskOnIterator mask_itr, const FixedRootGrid2* parent) :
        mask_itr_(mask_itr),
        parent_(parent)
    { }

    ChildType* get_child_ptr() const {
      return parent_->child_grid_[mask_itr_.offset()];
    }

    ChildType& get_child() const { return *this->get_child_ptr(); }

    bool test() const { return mask_itr_.test(); }
    operator bool() const { return this->test(); }
    bool done() const { return !this->test(); }

    bool next() { this->increment(); return this->test(); }

    void increment() { mask_itr_.increment(); }

    ChildOnIterator& operator++() { this->increment(); return *this; }
    ChildType& operator*() const { return this->get_child(); }
    ChildType* operator->() const { return &(this->operator*()); }

    VecIx get_local_grid_ix() const {
      return parent_->offset_to_local_grid(mask_itr_.offset());
    }

    mix_t get_offset() const { return mask_itr_.offset(); }

    BitMaskOnIterator mask_itr_;
    const FixedRootGrid2* parent_;
  };

  friend class ChildOnIterator;

  ChildOnIterator begin_child_on() const {
    return ChildOnIterator(child_mask_.beginOn(), this);
  }

public:
  struct ValueOnIterator {
    // TODO write this using childiterator
    ValueOnIterator() {}

    ValueOnIterator(BitMaskOnIterator mask_itr, FixedRootGrid2* parent) :
        mask_itr_(mask_itr),
        parent_(parent)
    {
      // get first valid child

      if (!this->done()) {
        mix_t mix = mask_itr_.offset();
        child_itr_ = parent_->child_grid_[mix]->begin_value_on();
      }
    }

    CellType& get_value() const { return child_itr_.get_value(); }
    CellType* get_value_ptr() const { return child_itr_.get_value_ptr(); }

    // Return true if this iterator is not yet exhausted.
    bool test() const { return mask_itr_.test(); }
    operator bool() const { return this->test(); }
    bool done() const { return !this->test(); }

    bool next() { this->increment(); return this->test(); }

    void increment() {
      if (this->done()) { return; }
      child_itr_.increment();
      //std::cerr << "child_itr_.test() = " << (int)child_itr_.test() << std::endl;
      if (child_itr_.done()) {
        // child_itr is exhausted, get next child
        mask_itr_.increment();
        if (!mask_itr_.test()) { return; }
        mix_t mix = mask_itr_.offset();
        ChildType * child = parent_->child_grid_[mix];
        child_itr_ = child->begin_value_on();
      }
    }

    ValueOnIterator& operator++() { this->increment(); return *this; }
    CellType& operator*() const { return this->get_value(); }
    CellType* operator->() const { return &(this->operator*()); }

    void set_value(const CellType& value) { child_itr_.set_value(value); }
    VecIx get_grid_ix() const { return child_itr_.get_grid_ix(); }

    BitMaskOnIterator mask_itr_;
    mutable FixedRootGrid2* parent_;
    typename ChildType::ValueOnIterator child_itr_;
  };

  ValueOnIterator begin_value_on() {
    return ValueOnIterator(child_mask_.beginOn(), this);
  }

  struct Accessor {
    // super simple "cached" accessor.

    Accessor() {}

    Accessor(FixedRootGrid2* parent) :
        parent_(parent),
        last_mix_(0),
        last_child_(nullptr),
        valid_(false)
    { }

    bool is_cache_hit(mix_t mix) { return (valid_ && (mix == last_mix_)); }

    void cache(mix_t mix, ChildType* childptr) {
      last_mix_ = mix;
      last_child_ = childptr;
      valid_ = true;
    }

    ChildType* get_child_ptr(mix_t mix) {
      // TODO is it faster to test bits than check
      // if entry is null ptr?
      return (parent_->child_mask_.isOn(mix)) ?
          parent_->child_grid_[mix] :
          nullptr;
    }

    bool probe(const VecIx& gix) {
      mix_t mix = parent_->grid_to_offset(gix);
      if (this->is_cache_hit(mix)) {
        return (last_child_ == nullptr) ? false : last_child_->probe(gix);
      } else {
        last_mix_ = mix;
        valid_ = true;
        last_child_ = this->get_child_ptr(mix);
        return (last_child_ == nullptr) ? false : last_child_->probe(gix);
      }
    }

    bool probe_value(const VecIx& gix, CellType& cell) {
      mix_t mix = parent_->grid_to_offset(gix);
      if (this->is_cache_hit(mix)) {
        return (last_child_ == nullptr) ? false : last_child_->probe_value(gix, cell);
      } else {
        last_mix_ = mix;
        valid_ = true;
        last_child_ = this->get_child_ptr(mix);
        return (last_child_ == nullptr) ? false : last_child_->probe_value(gix, cell);
      }
    }

    const CellType& get_value(const VecIx& gix) {
      mix_t mix = parent_->grid_to_offset(gix);
      if (this->is_cache_hit(mix)) {
        return (last_child_ == nullptr) ?
            parent_->background() :
            last_child_->get_value(gix);
      } else {
        last_mix_ = mix;
        valid_ = true;
        last_child_ = this->get_child_ptr(mix);
        return (last_child_ == nullptr) ?
            parent_->background() :
            last_child_->get_value(gix);
      }
    }

    CellType* get_value_ptr(const VecIx& gix) {
      mix_t mix = parent_->grid_to_offset(gix);
      if (this->is_cache_hit(mix)) {
        return (last_child_ == nullptr) ?
            nullptr :
            last_child_->get_value_ptr(gix);
      } else {
        last_mix_ = mix;
        valid_ = true;
        last_child_ = this->get_child_ptr(mix);
        return (last_child_ == nullptr) ?
            nullptr :
            last_child_->get_value_ptr(gix);
      }
    }

    void set_value(const VecIx& gix, const CellType& val) {
      mix_t mix = parent_->grid_to_offset(gix);
      if (this->is_cache_hit(mix)) {
        if (last_child_ == nullptr) {
          last_child_ = parent_->insert_child_with_value(mix, gix, val);
        } else {
          last_child_->set_value(gix, val);
        }
      } else {
        last_mix_ = mix;
        valid_ = true;
        last_child_ = this->get_child_ptr(mix);
        if (last_child_ == nullptr) {
          last_child_ = parent_->insert_child_with_value(mix, gix, val);
        } else {
          last_child_->set_value(gix, val);
        }
      }
    }

    FixedRootGrid2* parent_ = nullptr;
    mix_t last_mix_ = 0;
    ChildType* last_child_ = nullptr;
    bool valid_ = false;
  };

  // a bit ugly
  friend class Accessor;

  Accessor get_accesor() {
    return Accessor(this);
  }

private:
  ArrayType child_grid_;
  CellType background_;
  BitMask child_mask_;

  // TODO maybe keep a mask for all children,
  // could be used for testing
  //BitMask total_cell_mask_;
};

} /* tilegrid */

#endif /* end of include guard: FIXEDROOTGRID_H_KCNDPWVZ */
