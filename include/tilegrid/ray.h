#ifndef RAY_H_YABRBWIK
#define RAY_H_YABRBWIK

#include <tuple>

#include <Eigen/Core>
#include <Eigen/Dense>

namespace tilegrid {

template<typename Scalar>
class Ray2 {
 public:
  using Vec2 = Eigen::Matrix<Scalar, 1, 2>;

 public:
  /**
   * @param origin: origin of ray
   * @param direction: *unit* direction of ray
   *
   */
  Ray2(const Vec2& origin,
       const Vec2& direction) :
         origin(origin),
         direction(direction.normalized()),
         tmin(Scalar(0)),
         tmax(std::numeric_limits<Scalar>::max()),
         invdir(Scalar(1.) / direction.array()),
         sign(invdir.x() < 0,
              invdir.y() < 0)
  {
  }

 public:
  Vec2 point_at(Scalar t) const {
    return origin + t*direction;
  }

 public:
  Vec2 origin, direction;
  Scalar tmin, tmax; /// ray min and max distances
  Vec2 invdir; // for convenience in AABB intersection
  std::tuple<int, int> sign;
};

template<typename Scalar>
class Ray3 {
 public:
  using Vec3 = Eigen::Matrix<Scalar, 1, 3>;

 public:
  /**
   * @param origin: origin of ray
   * @param direction: *unit* direction of ray
   *
   */
  Ray3(const Vec3& origin,
       const Vec3& direction) :
         origin(origin),
         direction(direction.normalized()),
         tmin(Scalar(0)),
         tmax(std::numeric_limits<Scalar>::max()),
         invdir(Scalar(1.) / direction.array()),
         sign(invdir.x() < 0,
              invdir.y() < 0,
              invdir.z() < 0)
  {
  }

 public:
  Vec3 point_at(Scalar t) const {
    return origin + t*direction;
  }

 public:
  Vec3 origin, direction;
  Scalar tmin, tmax; /// ray min and max distances
  Vec3 invdir; // for convenience in AABB intersection
  std::tuple<int, int, int> sign;

};

using Ray2f = Ray2<float>;
using Ray3f = Ray3<float>;
using Ray2d = Ray2<double>;
using Ray3d = Ray3<double>;

}

#endif /* end of include guard: RAY_H_YABRBWIK */
