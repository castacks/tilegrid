/**
 * @author  Daniel Maturana
 * @year    2018
 *
 * @attention Copyright (c) 2018
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 */

#ifndef _TILEGRID_TILEGRID_H_
#define _TILEGRID_TILEGRID_H_

#include "tilegrid/types.h"
#include "tilegrid/densegrid.h"
#include "tilegrid/rootgrid.h"
#include "tilegrid/fixedrootgrid.h"

#include <Eigen/Core>

namespace tilegrid {

template<class RootGridT>
class TileGrid2 {
public:
  using RootGridType = RootGridT;
  using CellType = typename RootGridT::CellType;
  using Vec = Eigen::Vector2f;
  using Vec3 = Eigen::Vector3f;
  using VecIx = Vec2Ix;
  using VecMIx = Vec2MIx;
  using MatIx = Mat2Ix;
  using MatS = Eigen::Matrix<float, Eigen::Dynamic, 2, Eigen::RowMajor>;
  using Ptr = std::shared_ptr<TileGrid2>;
  using ConstPtr = std::shared_ptr<const TileGrid2>;
  using ValueOnIterator = typename RootGridType::ValueOnIterator;
  using ChildOnIterator = typename RootGridType::ChildOnIterator;
  using Accessor = typename RootGridType::Accessor;
  using BoxType = typename RootGridType::BoxType;

  TileGrid2() :
      resolution_(0.),
      origin_(Vec::Zero())
  { }

  TileGrid2(float resolution) :
      resolution_(resolution),
      origin_(Vec::Zero())
  { }

  TileGrid2(float resolution,
            const Vec& origin) :
      resolution_(resolution),
      origin_(Vec::Zero())
  { }

  TileGrid2(float resolution,
            const Vec& origin,
            const CellType& background) :
      resolution_(resolution),
      origin_(origin),
      grid_(background)
  { }

  virtual ~TileGrid2() { }
  TileGrid2(const TileGrid2& other) = delete;
  TileGrid2& operator=(const TileGrid2& other) = delete;

public:
  void clear() { grid_.clear(); }

  void set_value(const VecIx& gix, const CellType& val) { grid_.set_value(gix, val); }

  // doesn't make voxels, only changes value
  void fill(const CellType& val) {
    grid_.fill(val);
  }

  // does not change values
  void set_inactive() {
    grid_.set_inactive();
  }

  const CellType& get_value(const VecIx& gix) { return grid_.get_value(gix); }

  bool probe(const VecIx& gix) { return grid_.probe(gix); }

  bool probe_value(const VecIx& gix, CellType& val) { return grid_.probe_value(gix, val); }

  ValueOnIterator begin_value_on() { return grid_.begin_value_on(); }

  ChildOnIterator begin_child_on() { return grid_.begin_child_on(); }

  Accessor get_accesor() { return grid_.get_accesor(); }

  mix_t active_cell_count() const { return grid_.active_cell_count(); }

  size_t allocated_bytes() const { return grid_.allocated_bytes(); }

  BoxType bbox() const { return grid_.bbox(); }

  VecIx min_grid_ix() const { return grid_.min_grid_ix(); }

  VecIx max_grid_ix() const { return grid_.max_grid_ix(); }

public:
  /**
   * Given position in world coordinates, return grid coordinates.
   */
  VecIx world_to_grid(const Vec& x) const {
    //Vec tmp = ((x - origin_).array() - 0.5*resolution_)/resolution_;
    //return VecIx(tmp.array().round().template cast<gix_t>());
    return VecIx(std::round((x[0] - origin_[0] - 0.5f*resolution_)/resolution_),
                 std::round((x[1] - origin_[1] - 0.5f*resolution_)/resolution_));
  }

  Vec grid_to_world(const VecIx& grid_ix) const {
    //Vec w((grid_ix.template cast<Scalar>()*resolution_ + origin_).array() + 0.5*resolution_);
    //Vec w((grid_ix.cast<float>()*resolution_ + origin_).array() + 0.5*resolution_);

    Vec w(grid_ix.x()*resolution_ + origin_.x() + 0.5f*resolution_,
          grid_ix.y()*resolution_ + origin_.y() + 0.5f*resolution_);
    return w;
  }

  MatS multi_grid_to_world(const MatIx& grid_indices) const {
    // TODO vectorize
    //MatS out(dim_t::value, grid_indices.rows());
    MatS out(grid_indices.rows(), grid_indices.cols());
    for (int ix=0; ix < grid_indices.rows(); ++ix) {
      VecIx gix(grid_indices.row(ix));
      out.row(ix) = this->grid_to_world(gix);
    }
    return out;
  }

public:
  void set_resolution(float r) {
    resolution_ = r;
  }

  float resolution() const {
    return resolution_;
  }

private:
  float resolution_;
  Vec origin_;
  RootGridType grid_;

};

} /* tilegrid */

#endif
