#ifndef DENSEGRID_H_ZYMGRXPS
#define DENSEGRID_H_ZYMGRXPS

#include <cstdint>
#include <iostream>
#include <array>
#include <stdexcept>
#include <algorithm>
#include <type_traits>
#include <memory>
#include <sstream>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Dense>

#include "tilegrid/types.h"
#include "tilegrid/node_masks.h"
#include "tilegrid/box.h"

namespace tilegrid
{

template<class CellT, mix_t Log2DimT>
class DenseGrid2 {
public:
  static constexpr mix_t Log2Dim = Log2DimT;
  static constexpr mix_t Dim = 1 << Log2DimT;
  static constexpr mix_t NumCells = 1 << (2*Log2DimT);

  using CellType = CellT;
  using ArrayType = std::array<CellType, NumCells>;
  using iterator = typename ArrayType::iterator;
  using const_iterator = typename ArrayType::const_iterator;
  using VecIx = Vec2Ix;
  using VecMIx = Vec2MIx;
  using MatIx = Mat2Ix;
  using Ptr = std::shared_ptr<DenseGrid2>;
  using ConstPtr = std::shared_ptr<const DenseGrid2>;
  using BitMask = NodeMask<Log2Dim, 2>;
  using BoxType = Box<gix_t, 2>;


public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  DenseGrid2() :
      origin_(VecIx::Zero()) {
  }

  DenseGrid2(const DenseGrid2& other) :
      origin_(other.origin_),
      cell_mask_(other.cell_mask_),
      grid_(other.grid_) {
  }

  // note we zero out lower order bits for origin
  DenseGrid2(const VecIx gix,
             const CellType& val,
             bool active) :
      origin_(gix[0] & (~(Dim - 1)),
              gix[1] & (~(Dim - 1))) {
    grid_.fill(val);
    cell_mask_.set(active);
  }

  DenseGrid2& operator=(const DenseGrid2& other) = delete;

  virtual ~DenseGrid2() { }

  void clear() {
    origin_.setZero();
    cell_mask_.setOff();

    CellType val();
    grid_.fill(val);
  }

  size_t allocated_bytes() const {
    return sizeof(*this) + (sizeof(CellT)*NumCells);
  }

  void fill(const CellT& val) {
    std::fill(this->begin(), this->end(), val);
  }

  void set_inactive() {
    //if (this->empty()) { return; }
    cell_mask_.setOff();
  }

  mix_t active_cell_count() const { return cell_mask_.countOn(); }

  bool empty() const { return cell_mask_.isOff(); }

  BoxType bbox() {
    BoxType box(this->min_grid_ix(), this->max_grid_ix());
    return box;
  }

  VecIx min_grid_ix() const {
    return origin_;
  }

  VecIx max_grid_ix() const {
    gix_t dim(static_cast<gix_t>(Dim));
    VecIx max_ix(origin_.x() + dim, origin_.y() + dim);
    return max_ix;
  }


public:
  /*
   * keep only lower (dim-1) bits note that higher bits are encoding global pos
   * strides = (2^Log2Dim, 1)
   */
  mix_t grid_to_offset(const VecIx& grid_ix) const {
    assert((grid_ix[0] & (Dim-1u)) < Dim &&
           (grid_ix[1] & (Dim-1u)) < Dim);
    return ((grid_ix[0] & (Dim-1u)) << Log2Dim)
          + (grid_ix[1] & (Dim-1u));
  }

  VecMIx multi_grid_to_offset(const MatIx& grid_indices) const {
    VecMIx out(grid_indices.rows());
    for (int ix=0; ix < grid_indices.rows(); ++ix) {
      VecIx gix(grid_indices.row(ix));
      out[ix] = this->grid_to_offset(gix);
    }
    return out;
  }

  VecIx offset_to_grid(mix_t mem_ix) const {
    assert(mem_ix < (1 << 2*Log2Dim));

    // if Log2Dim=2, Dim=4, then
    // lower 3 bits contain last coord,
    // then the 3 bits before that contain
    // next to last coord, etc.
    // e.g. mem_ix = 6 = 0110
    // then j = 10 = 2,
    // and i = 01 = 1

    // divide by 2^Log2Dim
    return VecIx(mem_ix >> Log2Dim,
                 mem_ix & ((1 << Log2Dim)-1));
  }

  VecIx offset_to_global_grid(mix_t mem_ix) const {
    return (this->offset_to_grid(mem_ix) + this->origin());
  }

  CellType& get_value(const VecIx& grid_ix) {
    mix_t mem_ix = this->grid_to_offset(grid_ix);
    return grid_[mem_ix];
  }

  CellType* get_value_ptr(const VecIx& grid_ix) {
    mix_t mem_ix = this->grid_to_offset(grid_ix);
    return &(grid_[mem_ix]);
  }

  void set_value(const VecIx& grid_ix, const CellType& val) {
    //std::cerr << "grid_ix[0] = " << grid_ix[0] << std::endl;
    //std::cerr << "grid_ix[1] = " << grid_ix[1] << std::endl;
    mix_t mem_ix = this->grid_to_offset(grid_ix);
    //std::cerr << "mem_ix = " << mem_ix << std::endl;
    grid_[mem_ix] = val;
    cell_mask_.setOn(mem_ix);
  }

  void set_value_only(const VecIx& grid_ix, const CellType& val) {
    mix_t mem_ix = this->grid_to_offset(grid_ix);
    grid_[mem_ix] = val;
  }

  CellType& get_value_offset(mix_t mem_ix) {
    return grid_[mem_ix];
  }

  CellType* get_value_ptr_offset(mix_t mem_ix) {
    return &(grid_[mem_ix]);
  }

  const CellType& get_value_offset(mix_t mem_ix) const {
    return grid_[mem_ix];
  }

  bool probe(const VecIx& grid_ix) const {
    mix_t mem_ix = this->grid_to_offset(grid_ix);
    return probe_offset(mem_ix);
  }

  bool probe_offset(mix_t mem_ix) const {
    return cell_mask_.isOn(mem_ix);
  }

  bool probe_value(const VecIx& grid_ix, CellType& val) const {
    mix_t mem_ix = this->grid_to_offset(grid_ix);
    return probe_value_offset(mem_ix, val);
  }

  bool probe_value_offset(mix_t mem_ix, CellType& val) const {
    bool active = cell_mask_.isOn(mem_ix);
    if (!active) {
      return false;
    }
    val = grid_[mem_ix];
    return true;
  }

#if 0
  /**
   * Bound check
   */
  CellType& get_safe(gix_t mem_ix) {
    if (mem_ix < 0 || mem_ix >= num_cells_) {
      throw std::out_of_range("bad mem_ix");
    }
    return grid_[mem_ix];
  }

  /**
   * Bound check
   */
  const CellType& get_safe(gix_t mem_ix) const {
    if (mem_ix < 0 || mem_ix >= num_cells_) {
      throw std::out_of_range("bad mem_ix");
    }
    return grid_[mem_ix];
  }
#endif

public:
  // simple interators
  iterator begin() { return grid_.begin(); }
  iterator end() { return grid_.end(); }

  const_iterator cbegin() const { return grid_.cbegin(); }
  const_iterator cend() const { return grid_.cend(); }

  struct ValueOnIterator {
    ValueOnIterator() {}
    ValueOnIterator(typename BitMask::OnIterator mask_iter, DenseGrid2* parent) :
        mask_iter_(mask_iter),
        parent_(parent) {
    }

    DenseGrid2& parent() const { return *parent_; }

    mix_t pos() const { return mask_iter_.offset(); }

    // TODO not sure what to use this one for.
    CellType& get_item(mix_t pos) const { return this->parent().get_value_offset(pos); }
    CellType& get_value() const { return this->parent().get_value_offset(this->pos()); }
    CellType* get_value_ptr() const { return this->parent().get_value_ptr_offset(); }

    // Note: setItem() can't be called on const iterators.
    void set_item(mix_t pos, const CellType& val) const {
      this->parent().set_value_only(pos, val);
    }

    // Note: setValue() can't be called on const iterators.
    void set_value(const CellType& val) const {
      this->parent().set_value_only(this->pos(), val);
    }

    // Return a reference to the item to which this iterator is pointing.
    CellType& operator*() const { return this->get_value(); }

    // Return a pointer to the item to which this iterator is pointing.
    CellType* operator->() const { return &(this->operator*()); }

    // Return true if this iterator is not yet exhausted.
    bool test() const { return mask_iter_.test(); }

    operator bool() const { return this->test(); }

    bool done() const { return !this->test(); }

    // Advance to the next item in the parent node's table.
    bool next() { return mask_iter_.next(); }
    void increment() { mask_iter_.increment(); }

    // Return the coordinates of the item to which this iterator is pointing.
    VecIx get_grid_ix() const { return parent().offset_to_global_grid(this->pos()); }

    typename BitMask::OnIterator mask_iter_;
    mutable DenseGrid2* parent_;
  };

  ValueOnIterator begin_value_on() {
    return ValueOnIterator(cell_mask_.beginOn(), this);
  }


public:
  // properties
  constexpr mix_t dim(int i) const { return Dim; }
  constexpr mix_t ndim() { return 2; }
  VecIx dimension() const { return VecIx(Dim, Dim); }
  constexpr mix_t num_cells() { return NumCells; }

  const VecIx& origin() const {
    return origin_;
  }

  ArrayType data() const { return &grid_[0]; }

  gix_t stride(int i) const {
    return (i == 0) ? (1 << Log2Dim) : 1;
  }

  VecIx strides() const { return VecIx(1 << Log2Dim, 1); }

private:
  ArrayType grid_;
  BitMask cell_mask_;

  // TODO paper says they quantize/bitpack this into a 64 bit int.
  // however, openvdb code doesn't do this.
  // technically this is wasting some space, as only upper bits are used.
  // (lower Dim-1 bits are 0).
  VecIx origin_;
};

} /* tilegrid */

#endif /* end of include guard: DENSEGRID_H_ZYMGRXPS */
