#ifndef NODE_MASKS_H_NJO0RHHX
#define NODE_MASKS_H_NJO0RHHX

/**
 * this is from openvdb.
 * consider also another class such as ClaasBontus/bitset2.
 */

#include <algorithm>
#include <cassert>
#include <cstring>
#include <iostream>

#include "tilegrid/types.h"

namespace tilegrid
{

/// Return the number of on bits in the given 8-bit value.
inline uint32_t CountOn(uint8_t v) {
  // Simple LUT:
  static const uint8_t numBits[256] = {
#define COUNTONB2(n)  n,            n+1,            n+1,            n+2
#define COUNTONB4(n)  COUNTONB2(n), COUNTONB2(n+1), COUNTONB2(n+1), COUNTONB2(n+2)
#define COUNTONB6(n)  COUNTONB4(n), COUNTONB4(n+1), COUNTONB4(n+1), COUNTONB4(n+2)
        COUNTONB6(0), COUNTONB6(1), COUNTONB6(1),   COUNTONB6(2)
  };
  return numBits[v];
#undef COUNTONB6
#undef COUNTONB4
#undef COUNTONB2
}

/// Return the number of off bits in the given 8-bit value.
inline uint32_t CountOff(uint8_t v) { return CountOn(static_cast<uint8_t>(~v)); }

/// Return the number of on bits in the given 32-bit value.
inline uint32_t CountOn(uint32_t v) {
  v = v - ((v >> 1) & 0x55555555U);
  v = (v & 0x33333333U) + ((v >> 2) & 0x33333333U);
  return (((v + (v >> 4)) & 0xF0F0F0FU) * 0x1010101U) >> 24;
}

/// Return the number of off bits in the given 32-bit value.
inline uint32_t CountOff(uint32_t v) { return CountOn(~v); }

/// Return the number of on bits in the given 64-bit value.
inline uint32_t CountOn(uint64_t v) {
  v = v - ((v >> 1) & UINT64_C(0x5555555555555555));
  v = (v & UINT64_C(0x3333333333333333)) + ((v >> 2) & UINT64_C(0x3333333333333333));
  return static_cast<uint32_t>(
      (((v + (v >> 4)) & UINT64_C(0xF0F0F0F0F0F0F0F)) * UINT64_C(0x101010101010101)) >> 56);
}

/// Return the number of off bits in the given 64-bit value.
inline uint32_t CountOff(uint64_t v) { return CountOn(~v); }

/// Return the least significant on bit of the given 8-bit value.
inline uint32_t FindLowestOn(uint8_t v) {
  assert(v);
  static const uint8_t DeBruijn[8] = {0, 1, 6, 2, 7, 5, 4, 3};
  return DeBruijn[uint8_t((v & -v) * 0x1DU) >> 5];
}

/// Return the least significant on bit of the given 32-bit value.
inline uint32_t FindLowestOn(uint32_t v) {
  assert(v);
  static const uint8_t DeBruijn[32] = {
    0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
    31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
  };
  return DeBruijn[uint32_t((v & -v) * 0x077CB531U) >> 27];
}

/// Return the least significant on bit of the given 64-bit value.
inline uint32_t FindLowestOn(uint64_t v) {
  assert(v);
  //return ffsll(v);
  static const uint8_t DeBruijn[64] = {
        0,   1,  2, 53,  3,  7, 54, 27, 4,  38, 41,  8, 34, 55, 48, 28,
        62,  5, 39, 46, 44, 42, 22,  9, 24, 35, 59, 56, 49, 18, 29, 11,
        63, 52,  6, 26, 37, 40, 33, 47, 61, 45, 43, 21, 23, 58, 17, 10,
        51, 25, 36, 32, 60, 20, 57, 16, 50, 31, 19, 15, 30, 14, 13, 12,
  };
  return DeBruijn[uint64_t((v & -v) * UINT64_C(0x022FDD63CC95386D)) >> 58];
}

/// Return the most significant on bit of the given 32-bit value.
inline uint32_t FindHighestOn(uint32_t v) {
  static const uint8_t DeBruijn[32] = {
    0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
    8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
  };
  v |= v >> 1; // first round down to one less than a power of 2
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  return DeBruijn[uint32_t(v * 0x07C4ACDDU) >> 27];
}

/// Base class for the bit mask iterators
template<typename NodeMask>
class BaseMaskIterator {
protected:
  uint32_t mPos; // bit position
  const NodeMask* mParent; // this iterator can't change the parent_mask!

public:
  BaseMaskIterator(): mPos(NodeMask::SIZE), mParent(nullptr) {}
  BaseMaskIterator(const BaseMaskIterator&) = default;
  BaseMaskIterator(uint32_t pos, const NodeMask* parent): mPos(pos), mParent(parent) {
    assert((parent == nullptr && pos == 0) || (parent != nullptr && pos <= NodeMask::SIZE));
  }
  bool operator==(const BaseMaskIterator &iter) const {return mPos == iter.mPos;}
  bool operator!=(const BaseMaskIterator &iter) const {return mPos != iter.mPos;}
  bool operator< (const BaseMaskIterator &iter) const {return mPos <  iter.mPos;}
  BaseMaskIterator& operator=(const BaseMaskIterator& iter) {
    mPos = iter.mPos; mParent = iter.mParent; return *this;
  }
  uint32_t offset() const { return mPos; }
  uint32_t pos() const { return mPos; }
  bool test() const { assert(mPos <= NodeMask::SIZE); return (mPos != NodeMask::SIZE); }
  operator bool() const { return this->test(); }
}; // class BaseMaskIterator


/// @note This happens to be a const-iterator!
template <typename NodeMask>
class OnMaskIterator: public BaseMaskIterator<NodeMask> {
private:
  using BaseType = BaseMaskIterator<NodeMask>;
  using BaseType::mPos;//bit position;
  using BaseType::mParent;//this iterator can't change the parent_mask!
public:
  OnMaskIterator() : BaseType() {}
  OnMaskIterator(uint32_t pos,const NodeMask *parent) : BaseType(pos,parent) {}
  void increment() {
    assert(mParent != nullptr);
    mPos = mParent->findNextOn(mPos+1);
    assert(mPos <= NodeMask::SIZE);
  }
  void increment(uint32_t n) { while(n-- && this->next()) ; }
  bool next() {
    this->increment();
    return this->test();
  }
  bool operator*() const {return true;}
  OnMaskIterator& operator++() {
    this->increment();
    return *this;
  }
}; // class OnMaskIterator


template <typename NodeMask>
class OffMaskIterator: public BaseMaskIterator<NodeMask> {
private:
  using BaseType = BaseMaskIterator<NodeMask>;
  using BaseType::mPos;//bit position;
  using BaseType::mParent;//this iterator can't change the parent_mask!
public:
  OffMaskIterator() : BaseType()  {}
  OffMaskIterator(uint32_t pos,const NodeMask *parent) : BaseType(pos,parent) {}
  void increment() {
    assert(mParent != nullptr);
    mPos=mParent->findNextOff(mPos+1);
    assert(mPos <= NodeMask::SIZE);
  }
  void increment(uint32_t n) { while(n-- && this->next()) ; }
  bool next() {
    this->increment();
    return this->test();
  }
  bool operator*() const {return false;}
  OffMaskIterator& operator++() {
    this->increment();
    return *this;
  }
}; // class OffMaskIterator


template <typename NodeMask>
class DenseMaskIterator: public BaseMaskIterator<NodeMask> {
 private:
  using BaseType = BaseMaskIterator<NodeMask>;
  using BaseType::mPos;//bit position;
  using BaseType::mParent;//this iterator can't change the parent_mask!

 public:
  DenseMaskIterator() : BaseType() {}
  DenseMaskIterator(uint32_t pos,const NodeMask *parent) : BaseType(pos,parent) {}
  void increment() {
    assert(mParent != nullptr);
    mPos += 1;//careful - the increment might go beyond the end
    assert(mPos<= NodeMask::SIZE);
  }
  void increment(uint32_t n) { while(n-- && this->next()) ; }
  bool next() {
    this->increment();
    return this->test();
  }
  bool operator*() const {return mParent->isOn(mPos);}
  DenseMaskIterator& operator++() {
    this->increment();
    return *this;
  }
}; // class DenseMaskIterator


/// @brief Bit mask for the internal and leaf nodes of VDB. This
/// is a 64-bit implementation.
///
/// @note only for Log2Dim >= 3
/// DMS: added NDim parameter for 2D/3D
//
template<mix_t Log2Dim, mix_t NDim>
class NodeMask {
public:
  static_assert(Log2Dim > 2, "expected NodeMask template specialization, got base template");

  static constexpr uint32_t LOG2DIM    = Log2Dim;
  static constexpr uint32_t NDIM       = NDim;
  static constexpr uint32_t DIM        = 1<<Log2Dim;
  static constexpr uint32_t SIZE       = 1<<(NDIM*Log2Dim);
  static constexpr uint32_t WORD_COUNT = SIZE >> 6; // 2^6=64
  using Word = uint64_t;

private:
  // The bits are represented as a linear array of Words, and the
  // size of a Word is 32 or 64 bits depending on the platform.
  // The BIT_MASK is defined as the number of bits in a Word - 1
  //static const uint32_t BIT_MASK   = sizeof(void*) == 8 ? 63 : 31;
  //static const uint32_t LOG2WORD   = BIT_MASK == 63 ? 6 : 5;
  //static const uint32_t WORD_COUNT = SIZE >> LOG2WORD;
  //using Word = boost::mpl::if_c<BIT_MASK == 63, uint64_t, uint32_t>::type;

  Word words_[WORD_COUNT]; //only member data!

public:
  /// Default constructor sets all bits off
  NodeMask() { this->setOff(); }
  /// All bits are set to the specified state
  NodeMask(bool on) { this->set(on); }
  /// Copy constructor
  NodeMask(const NodeMask &other) { *this = other; }
  /// Destructor
  ~NodeMask() {}
  /// Assignment operator
  NodeMask& operator=(const NodeMask& other) {
    uint32_t n = WORD_COUNT;
    const Word* w2 = other.words_;
    for (Word* w1 = words_; n--; ++w1, ++w2) *w1 = *w2;
    return *this;
  }

  using OnIterator = OnMaskIterator<NodeMask>;
  using OffIterator = OffMaskIterator<NodeMask>;
  using DenseIterator = DenseMaskIterator<NodeMask>;

  OnIterator beginOn() const       { return OnIterator(this->findFirstOn(),this); }
  OnIterator endOn() const         { return OnIterator(SIZE,this); }
  OffIterator beginOff() const     { return OffIterator(this->findFirstOff(),this); }
  OffIterator endOff() const       { return OffIterator(SIZE,this); }
  DenseIterator beginDense() const { return DenseIterator(0,this); }
  DenseIterator endDense() const   { return DenseIterator(SIZE,this); }

  bool operator == (const NodeMask &other) const {
    int n = WORD_COUNT;
    for (const Word *w1=words_, *w2=other.words_; n-- && *w1++ == *w2++;) ;
    return n == -1;
  }

  bool operator != (const NodeMask &other) const { return !(*this == other); }

  //
  // Bitwise logical operations
  //

  /// @brief Apply a functor to the words of the this and the other mask.
  ///
  /// @details An example that implements the "operator&=" method:
  /// @code
  /// struct Op { inline void operator()(W &w1, const W& w2) const { w1 &= w2; } };
  /// @endcode
  template<typename WordOp>
  const NodeMask& foreach(const NodeMask& other, const WordOp& op) {
    Word *w1 = words_;
    const Word *w2 = other.words_;
    for (uint32_t n = WORD_COUNT; n--;  ++w1, ++w2) op( *w1, *w2);
    return *this;
  }

  template<typename WordOp>
  const NodeMask& foreach(const NodeMask& other1, const NodeMask& other2, const WordOp& op) {
    Word *w1 = words_;
    const Word *w2 = other1.words_, *w3 = other2.words_;
    for (uint32_t n = WORD_COUNT; n--;  ++w1, ++w2, ++w3) op( *w1, *w2, *w3);
    return *this;
  }

  template<typename WordOp>
  const NodeMask& foreach(const NodeMask& other1, const NodeMask& other2, const NodeMask& other3,
                          const WordOp& op) {
    Word *w1 = words_;
    const Word *w2 = other1.words_, *w3 = other2.words_, *w4 = other3.words_;
    for (uint32_t n = WORD_COUNT; n--;  ++w1, ++w2, ++w3, ++w4) op(*w1, *w2, *w3, *w4);
    return *this;
  }

  /// @brief Bitwise intersection
  const NodeMask& operator&=(const NodeMask& other) {
    Word *w1 = words_;
    const Word *w2 = other.words_;
    for (uint32_t n = WORD_COUNT; n--;  ++w1, ++w2) *w1 &= *w2;
    return *this;
  }

  /// @brief Bitwise union
  const NodeMask& operator|=(const NodeMask& other) {
    Word *w1 = words_;
    const Word *w2 = other.words_;
    for (uint32_t n = WORD_COUNT; n--;  ++w1, ++w2) *w1 |= *w2;
    return *this;
  }

  /// @brief Bitwise difference
  const NodeMask& operator-=(const NodeMask& other) {
    Word *w1 = words_;
    const Word *w2 = other.words_;
    for (uint32_t n = WORD_COUNT; n--;  ++w1, ++w2) *w1 &= ~*w2;
    return *this;
  }

  /// @brief Bitwise XOR
  const NodeMask& operator^=(const NodeMask& other) {
    Word *w1 = words_;
    const Word *w2 = other.words_;
    for (uint32_t n = WORD_COUNT; n--;  ++w1, ++w2) *w1 ^= *w2;
    return *this;
  }

  NodeMask operator!()                      const { NodeMask m(*this); m.toggle(); return m; }
  NodeMask operator&(const NodeMask& other) const { NodeMask m(*this); m &= other; return m; }
  NodeMask operator|(const NodeMask& other) const { NodeMask m(*this); m |= other; return m; }
  NodeMask operator^(const NodeMask& other) const { NodeMask m(*this); m ^= other; return m; }

  /// Return the byte size of this NodeMask
  static uint32_t memUsage() { return static_cast<uint32_t>(WORD_COUNT*sizeof(Word)); }

  /// Return the total number of on bits
  uint32_t countOn() const {
    uint32_t sum = 0, n = WORD_COUNT;
    for (const Word* w = words_; n--; ++w) { sum += CountOn(*w); }
    return sum;
  }

  /// Return the total number of on bits
  uint32_t countOff() const { return SIZE-this->countOn(); }

  /// Set the <i>n</i>th  bit on
  void setOn(uint32_t n) {
    assert( (n >> 6) < WORD_COUNT );
    words_[n >> 6] |=  Word(1) << (n & 63);
  }

  /// Set the <i>n</i>th bit off
  void setOff(uint32_t n) {
    assert( (n >> 6) < WORD_COUNT );
    words_[n >> 6] &=  ~(Word(1) << (n & 63));
  }

  /// Set the <i>n</i>th bit to the specified state
  void set(uint32_t n, bool On) { On ? this->setOn(n) : this->setOff(n); }

  /// Set all bits to the specified state
  void set(bool on) {
    const Word state = on ? ~Word(0) : Word(0);
    uint32_t n = WORD_COUNT;
    for (Word* w = words_; n--; ++w) *w = state;
  }

  /// Set all bits on
  void setOn() {
    uint32_t n = WORD_COUNT;
    for (Word* w = words_; n--; ++w) *w = ~Word(0);
  }

  /// Set all bits off
  void setOff() {
    uint32_t n = WORD_COUNT;
    for (Word* w = words_; n--; ++w) *w = Word(0);
  }

  /// Toggle the state of the <i>n</i>th bit
  void toggle(uint32_t n) {
    assert( (n >> 6) < WORD_COUNT );
    words_[n >> 6] ^= Word(1) << (n & 63);
  }

  /// Toggle the state of all bits in the mask
  void toggle() {
    uint32_t n = WORD_COUNT;
    for (Word* w = words_; n--; ++w) *w = ~*w;
  }

  /// Set the first bit on
  void setFirstOn()  { this->setOn(0); }

  /// Set the last bit on
  void setLastOn()   { this->setOn(SIZE-1); }

  /// Set the first bit off
  void setFirstOff() { this->setOff(0); }

  /// Set the last bit off
  void setLastOff()  { this->setOff(SIZE-1); }

  /// Return @c true if the <i>n</i>th bit is on
  bool isOn(uint32_t n) const {
    assert( (n >> 6) < WORD_COUNT );
    return 0 != (words_[n >> 6] & (Word(1) << (n & 63)));
  }

  /// Return @c true if the <i>n</i>th bit is off
  bool isOff(uint32_t n) const {return !this->isOn(n); }

  /// Return @c true if all the bits are on
  bool isOn() const {
    int n = WORD_COUNT;
    for (const Word *w = words_; n-- && *w++ == ~Word(0);) ;
    return n == -1;
  }

  /// Return @c true if all the bits are off
  bool isOff() const {
    int n = WORD_COUNT;
    for (const Word *w = words_; n-- && *w++ == Word(0);) ;
    return n == -1;
  }

  /// Return @c true if bits are either all off OR all on.
  /// @param isOn Takes on the values of all bits if the method
  /// returns true - else it is undefined.
  bool isConstant(bool &isOn) const {
    isOn = (words_[0] == ~Word(0));//first word has all bits on
    if ( !isOn && words_[0] != Word(0)) return false;//early out
    const Word *w = words_ + 1, *n = words_ + WORD_COUNT;
    while( w<n && *w == words_[0] ) ++w;
    return w == n;
  }

  uint32_t findFirstOn() const {
    uint32_t n = 0;
    const Word* w = words_;
    for (; n<WORD_COUNT && !*w; ++w, ++n) ;
    return n==WORD_COUNT ? SIZE : (n << 6) + FindLowestOn(*w);
  }

  uint32_t findFirstOff() const {
    uint32_t n = 0;
    const Word* w = words_;
    for (; n<WORD_COUNT && !~*w; ++w, ++n) ;
    return n==WORD_COUNT ? SIZE : (n << 6) + FindLowestOn(~*w);
  }

  //@{
  /// Return the <i>n</i>th word of the bit mask, for a word of arbitrary size.
  template<typename WordT>
  WordT getWord(mix_t n) const {
    assert(n*8*sizeof(WordT) < SIZE);
    return reinterpret_cast<const WordT*>(words_)[n];
  }

  template<typename WordT>
  WordT& getWord(mix_t n) {
    assert(n*8*sizeof(WordT) < SIZE);
    return reinterpret_cast<WordT*>(words_)[n];
  }
  //@}

  void printInfo(std::ostream& os=std::cout) const {
    os << "NodeMask: Dim=" << DIM << " Log2Dim=" << Log2Dim
        << " Bit count=" << SIZE << " word count=" << WORD_COUNT << std::endl;
  }

  void printBits(std::ostream& os=std::cout, uint32_t max_out=80u) const {
    const uint32_t n=(SIZE>max_out ? max_out : SIZE);
    for (uint32_t i=0; i < n; ++i) {
      if ( !(i & 63) )
        os << "||";
      else if ( !(i%8) )
        os << "|";
      os << this->isOn(i);
    }
    os << "|" << std::endl;
  }

  void printAll(std::ostream& os=std::cout, uint32_t max_out=80u) const {
    this->printInfo(os);
    this->printBits(os, max_out);
  }

  uint32_t findNextOn(uint32_t start) const {
    uint32_t n = start >> 6;//initiate
    if (n >= WORD_COUNT) return SIZE; // check for out of bounds
    uint32_t m = start & 63;
    Word b = words_[n];
    if (b & (Word(1) << m)) return start;//simpel case: start is on
    b &= ~Word(0) << m;// mask out lower bits
    while(!b && ++n<WORD_COUNT) b = words_[n];// find next none-zero word
    return (!b ? SIZE : (n << 6) + FindLowestOn(b));//catch last word=0
  }

  uint32_t findNextOff(uint32_t start) const {
    uint32_t n = start >> 6;//initiate
    if (n >= WORD_COUNT) return SIZE; // check for out of bounds
    uint32_t m = start & 63;
    Word b = ~words_[n];
    if (b & (Word(1) << m)) return start;//simpel case: start is on
    b &= ~Word(0) << m;// mask out lower bits
    while(!b && ++n<WORD_COUNT) b = ~words_[n];// find next none-zero word
    return (!b ? SIZE : (n << 6) + FindLowestOn(b));//catch last word=0
  }
};// NodeMask


} /* tilegrid */

#endif /* end of include guard: NODE_MASKS_H_NJO0RHHX */
