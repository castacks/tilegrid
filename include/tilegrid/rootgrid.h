#ifndef ROOTGRID_H_4C7EZ8LF
#define ROOTGRID_H_4C7EZ8LF

#include <cstdint>
#include <memory>
#include <sstream>
#include <iostream>
#include <bitset>
#include <unordered_map>
#include <map>
#include <set>
#include <tuple>

#include <ros/console.h>

#include "tilegrid/flat_map.hpp"
#include "tilegrid/types.h"


namespace tilegrid
{

template<typename ChildGridT>
class RootGrid2 {
public:
  using ChildType = ChildGridT;
  using CellType = typename ChildType::CellType;

  using VecIx = Vec2Ix;
  using VecMIx = Vec2MIx;
  using MatIx = Mat2Ix;
  using Ptr = std::shared_ptr<RootGrid2>;
  using ConstPtr = std::shared_ptr<const RootGrid2>;

  using RootKey = std::tuple<std::int32_t, std::int32_t>;
  using HashKey = std::uint32_t;
  using BoxType = typename ChildType::BoxType;

private:
  //using MapType = std::map<RootKey, ChildType*>;
  using MapType = chobo::flat_map<RootKey, ChildType*>;
  using MapIter = typename MapType::iterator;
  using MapCIter = typename MapType::const_iterator;
  using RootKeySet = std::set<RootKey>;
  using RootKeySetIter = typename RootKeySet::iterator;
  using RootKeySetCIter = typename RootKeySet::const_iterator;

public:

  RootGrid2() : background_() { }

  RootGrid2(const CellType& background) :
      background_(background) {
  }

  virtual ~RootGrid2() {
    this->clear();
  }

  void clear() {
    for (MapIter itr = table_.begin(), end_itr = table_.end();
         itr != end_itr;
         ++itr) {
      if (itr->second) { delete itr->second; itr->second = nullptr; }
    }
    table_.clear();
  }

  RootGrid2(const RootGrid2& other) = delete;
  RootGrid2& operator=(const RootGrid2& other) = delete;

  bool empty() const {
    if (table_.empty()) {
      return true;
    }
    // if table is nonempty, check if all children are null
    for (MapCIter itr = table_.begin(), end_itr = table_.end();
         itr != end_itr;
         ++itr) {
      if (itr->second != nullptr) { return false; }
    }
    return true;
  }

  /**
   * openvdb either uses hash, or lexicographical ordering
   * keyed by tuples of quantized (x, y, z)
   */
  static RootKey grid_to_key(const VecIx& gix) {
    // zero out lower order bits
    return RootKey(gix[0] & ~(ChildType::Dim - 1),
                   gix[1] & ~(ChildType::Dim - 1));
  }

  /**
   * In case we want to go the spatial hash route.
   */
  static HashKey grid_to_hash(const VecIx& gix) {
    VecIx key(gix[0] & ~(ChildType::Dim - 1),
              gix[1] & ~(ChildType::Dim - 1));
    constexpr std::uint32_t Log2N = 10;
    constexpr std::uint32_t Log2NConst = (1 << Log2N)-1;
    // z const is 83492791
    return (Log2NConst &
            key[0]*73856093 ^
            key[1]*19349663);
  }

  MapIter find_grid_ix(const VecIx& gix) { return table_.find(grid_to_key(gix)); }
  MapCIter find_grid_ix(const VecIx& gix) const { return table_.find(grid_to_key(gix)); }

  MapIter find_key(const RootKey& key) { return table_.find(key); }
  MapCIter find_key(const RootKey& key) const { return table_.find(key); }

  // TODO don't expose this
  //MapCIter table_end() { return table_.

  const CellType& get_value(const VecIx& gix) const {
    MapCIter itr = this->find_grid_ix(gix);
    if (itr == table_.end() || itr->second == nullptr)  {
      return background_;
    }
    return itr->second->get_value(gix);
  }

  /*
   * May return nullptr. Use with caution!
   */
  ChildType* get_child(const VecIx& gix) const {
    MapCIter itr = this->find_grid_ix(gix);
    if (itr == table_.end() || itr->second == nullptr)  {
      return nullptr;
    }
    return itr->second;
  }

  /**
   * Only use if you know you need it
   */
  ChildType* insert_child_with_value(const RootKey& key, const VecIx& gix, const CellType& val) {
    ChildType *child(new ChildType(gix, background_, false));
    child->set_value(gix, val);
    table_[key] = child;
    return child;
  }

  void set_value(const VecIx& gix, const CellType& val) {
    // TODO consider updating metadata here.

    ChildType* child = nullptr;
    MapIter itr(this->find_grid_ix(gix));
    if (itr == table_.end() || itr->second == nullptr) {
      child = new ChildType(gix, background_, false);
      RootKey key(this->grid_to_key(gix));
      //std::cerr << "set_value key = " << std::get<0>(key) << ", " << std::get<1>(key) << "\n";
      table_[key] = child;
    } else {
      child = itr->second;
    }
    if (child) {
      child->set_value(gix, val);
    }
  }

  bool probe_value(const VecIx& gix, CellType& val) const {
    MapCIter itr = this->find_grid_ix(gix);
    if (itr == table_.end() || itr->second == nullptr) {
      val = background_;
      return false;
    }
    return itr->second->probe_value(gix, val);
  }

  bool probe(const VecIx& gix) const {
    MapCIter itr(this->find_grid_ix(gix));
    if (itr == table_.end() || itr->second == nullptr) {
      return false;
    }
    return itr->second->probe(gix);
  }

  mix_t active_cell_count() const {
    // TODO cache this
    mix_t count(0);
    for (MapCIter itr = table_.begin();
         itr != table_.end();
         ++itr) {
      if (const ChildType *child = itr->second) {
        count += child->active_cell_count();
      }
    }
    return count;
  }

  size_t allocated_bytes() const {
    // TODO cache
    size_t sz(sizeof(*this));
    for (MapCIter itr = table_.begin();
         itr != table_.end();
         ++itr) {
      if (const ChildType *child = itr->second) {
        sz += child->allocated_bytes();
      }
    }
    return sz;
  }

  VecIx min_grid_ix() const {
    // TODO cache
    if (this->empty()) { return VecIx::Zero(); }
    VecIx min_ix = VecIx::Constant(std::numeric_limits<gix_t>::max());
    for (MapCIter itr = table_.begin();
         itr != table_.end();
         ++itr) {
      if (const ChildType *child = itr->second) {
        VecIx mi(child->min_grid_ix());
        min_ix = min_ix.array().min(mi.array());
      }
    }
    return min_ix;
  }

  VecIx max_grid_ix() const {
    // TODO cache
    if (this->empty()) { return VecIx::Zero(); }
    VecIx max_ix = VecIx::Constant(std::numeric_limits<gix_t>::min());
    for (MapCIter itr = table_.begin();
         itr != table_.end();
         ++itr) {
      if (const ChildType *child = itr->second) {
        VecIx Mi(child->max_grid_ix());
        max_ix = max_ix.array().max(Mi.array());
      }
    }
    return max_ix;
  }

  BoxType bbox() const {
    // TODO cache
    if (this->empty()) { return BoxType(); }
    VecIx min_ix = VecIx::Constant(std::numeric_limits<gix_t>::max());
    VecIx max_ix = VecIx::Constant(std::numeric_limits<gix_t>::min());
    for (MapCIter itr = table_.begin();
         itr != table_.end();
         ++itr) {
      if (const ChildType *child = itr->second) {
        VecIx mi(child->min_grid_ix());
        VecIx Mi(child->max_grid_ix());
        min_ix = min_ix.array().min(mi.array());
        max_ix = max_ix.array().max(Mi.array());
      }
    }
    return BoxType(min_ix, max_ix);
  }

  const CellType& background() {
    return background_;
  }

public:
  // dummy code for now
  struct ChildOnIterator { };

  ChildOnIterator begin_child_on() {
    return ChildOnIterator();
  }

public:
  struct ValueOnIterator {
    // TODO
    // openvdb has a rootnodemask with
    // its own iterator
    ValueOnIterator() {}
    ValueOnIterator(MapIter map_itr,
                    MapIter map_itr_end) :
        map_itr_(map_itr),
        map_itr_end_(map_itr_end)
    {
      // find first valid map_itr
      while ((map_itr_->second == nullptr) && (!this->done())) {
        ++map_itr_;
      }

      if (this->done()) { return; }

      child_itr_ = map_itr_->second->begin_value_on();
    }

    // Return true if this iterator is not yet exhausted.
    bool test() const { return (map_itr_ != map_itr_end_); }
    operator bool() const { return this->test(); }
    bool done() const { return !this->test(); }

    bool next() {
      this->increment();
      return this->test();
    }

    void increment() {
      if (this->done()) { return; }

      child_itr_.increment();

      //std::cerr << "child_itr_.test() = " << (int)child_itr_.test() << std::endl;
      if (child_itr_.done()) {
        // child_itr is exhausted, get next child
        // seems like it's conceivable map_itr.second might be null.
        do {
          ++map_itr_;
        } while ((map_itr_->second == nullptr) && (!this->done()));

        if (this->done()) {
          // map_itr is exhausted too
          return;
        }

        child_itr_ = map_itr_->second->begin_value_on();
      }
    }

    ValueOnIterator& operator++() {
      this->increment();
      return *this;
    }

    CellType& get_value() const {
      return child_itr_.get_value();
    }

    CellType& operator*() const { return this->get_value(); }

    CellType* operator->() const { return &(this->operator*()); }

    void set_value(const CellType& value) { child_itr_.set_value(value); }

    VecIx get_grid_ix() const { return child_itr_.get_grid_ix(); }

    MapIter map_itr_, map_itr_end_;
    typename ChildType::ValueOnIterator child_itr_;
  };

  ValueOnIterator begin_value_on() {
    return ValueOnIterator(table_.begin(),
                           table_.end());
  }

  struct Accessor {
    // super simple "cached" accessor.

    Accessor(RootGrid2* parent) :
        parent_(parent),
        last_key_(0, 0),
        last_child_(nullptr),
        valid_(false)
    { }

    bool probe(const VecIx& gix) {
      RootKey key(grid_to_key(gix));
      if (valid_ && (key == last_key_)) {
        // cache hit
        return (last_child_ == nullptr) ? false : last_child_->probe(gix);
      } else {
        // cache miss
        MapCIter itr = parent_->table_.find(key);
        if ((itr == parent_->table_.end()) ||
            (itr->second == nullptr)) {
          // nothing here
          last_key_ = key;
          last_child_ = nullptr;
          valid_ = true;
          return false;
        }
        // found valid child, cache
        last_key_ = key;
        last_child_ = itr->second;
        valid_ = true;
        return last_child_->probe(gix);
      }
    }

    bool probe_value(const VecIx& gix, CellType& cell) {
      RootKey key(grid_to_key(gix));
      if (valid_ && (key == last_key_)) {
        // cache hit
        if (last_child_ == nullptr) {
          // TODO set cell to background?
          return false;
        } else {
          return last_child_->probe_value(gix, cell);
        }
      } else {
        // cache miss
        MapCIter itr = parent_->table_.find(key);
        if ((itr == parent_->table_.end()) ||
            (itr->second == nullptr)) {
          // nothing here
          last_key_ = key;
          last_child_ = nullptr;
          valid_ = true;
          return false;
        }
        // found valid child, cache
        last_key_ = key;
        last_child_ = itr->second;
        valid_ = true;
        return last_child_->probe_value(gix, cell);
      }
    }

    const CellType& get_value(const VecIx& gix) {
      RootKey key(grid_to_key(gix));
      if (valid_ && (key == last_key_)) {
        // cache hit
        return (last_child_ == nullptr) ?
            parent_->background() :
            last_child_->get_value(gix);
      } else {
        // cache miss
        MapCIter itr = parent_->table_.find(key);
        if ((itr == parent_->table_.end()) ||
            (itr->second == nullptr)) {
          // nothing here, return background
          last_key_ = key;
          last_child_ = nullptr;
          valid_ = true;
          return parent_->background();
        }
        // found valid child, cache
        last_key_ = key;
        last_child_ = itr->second;
        valid_ = true;
        return last_child_->get_value(gix);
      }
    }

    void set_value(const VecIx& gix, const CellType& val) {
      RootKey key(grid_to_key(gix));
      if (valid_ && (key == last_key_)) {
        // cache hit
        if (last_child_ == nullptr) {
          // no child, create and update cache
          last_child_ = parent_->insert_child_with_value(key, gix, val);
        } else {
          // insert into cached child
          last_child_->set_value(gix, val);
        }
      } else {
        // cache miss
        MapIter itr(parent_->table_.find(key));
        if (itr == parent_->table_.end() || itr->second == nullptr) {
          // no child, create and update cache
          last_child_ = parent_->insert_child_with_value(key, gix, val);
          last_key_ = key;
          valid_ = true;
        } else {
          // insert data in child and update cache
          last_child_ = itr->second;
          last_child_->set_value(gix, val);
          last_key_ = key;
          valid_ = true;
        }
      }
    }

    RootGrid2* parent_;
    RootKey last_key_;
    ChildType* last_child_;
    bool valid_;
  };

  // a bit ugly
  friend class Accessor;

  Accessor get_accesor() {
    return Accessor(this);
  }


private:
  MapType table_;
  CellType background_;

};

} /* tilegrid */

#endif /* end of include guard: ROOTGRID_H_4C7EZ8LF */
