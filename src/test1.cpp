#include "tilegrid/densegrid.h"
#include "tilegrid/rootgrid.h"
#include "tilegrid/fixedrootgrid.h"

#include "tilegrid/tilegrid.h"
#include "tilegrid/node_masks.h"

int main(int argc, char *argv[]) {

  using namespace tilegrid;

  using G = Vec2Ix;

  std::cerr << "test1\n";
  {
    RootGrid2<DenseGrid2<float, 3>> grid;

    grid.set_value(G(2, 28), 82.);
    grid.set_value(G(2, -28), 83.);
    grid.set_value(G(-42, 28), 84.);

    float v = grid.get_value(G(2, 28));
    std::cerr << "grid.probe(G(2, 28) = " << ((int)grid.probe(G(2, 28))) << std::endl;
    std::cerr << "v = " << v << std::endl;

    float v2 = grid.get_value(G(2, -28));
    std::cerr << "v2 = " << v2 << std::endl;

    float v3 = grid.get_value(G(-42, 28));
    std::cerr << "v3 = " << v3 << std::endl;

    std::cerr << "grid.probe(G(3, 4)) = " << ((int)grid.probe(G(3, 4))) << std::endl;
  }
  std::cerr << "****************************************\n";

  std::cerr << "tile grid2f dynamic \n\n";
  {

    using TileGrid2f = TileGrid2<RootGrid2<DenseGrid2<float, 3>>>;

    TileGrid2f grid(2.0);
    grid.set_value(G(2, 28), 82.);
    grid.set_value(G(2, -28), 83.);
    grid.set_value(G(-42, 28), 84.);

    float v = grid.get_value(G(2, 28));
    std::cerr << "grid.probe(G(2, 28) = " << ((int)grid.probe(G(2, 28))) << std::endl;
    std::cerr << "v = " << v << std::endl;

    float v2 = grid.get_value(G(2, -28));
    std::cerr << "v2 = " << v2 << std::endl;

    float v3 = grid.get_value(G(-42, 28));
    std::cerr << "v3 = " << v3 << std::endl;

    std::cerr << "grid.probe(G(3, 4)) = " << ((int)grid.probe(G(3, 4))) << std::endl;

    std::cerr << "grid.active_cell_count() = " << grid.active_cell_count() << std::endl;

    TileGrid2f::BoxType box(grid.bbox());
    std::cerr << "box.min_pt() = " << box.min_pt() << std::endl;
    std::cerr << "box.max_pt() = " << box.max_pt() << std::endl;

    for (TileGrid2f::ValueOnIterator itr = grid.begin_value_on();
         itr.test();
         ++itr) {
      float x = *itr;
      std::cerr << "x = " << x << std::endl;
    }

  }

  std::cerr << "****************************************\n";
  std::cerr << "tile grid2f fixed \n\n";
  {
    using DenseGrid2f = DenseGrid2<float, 3>;
    using FTileGrid2f = TileGrid2<FixedRootGrid2<DenseGrid2f, 3>>;

    // note that not sure how the negative indices work here since
    // values should probably be in (0, TotalDim-1)

    FTileGrid2f grid(2.0);
    grid.set_value(G(2, 28), 82.);
    grid.set_value(G(2, -28), 83.);
    grid.set_value(G(-42, 28), 84.);

    grid.set_value(G(-21, 4), 86.);

    float v = grid.get_value(G(2, 28));
    std::cerr << "grid.probe(G(2, 28) = " << ((int)grid.probe(G(2, 28))) << std::endl;
    std::cerr << "v = " << v << std::endl;

    float v2 = grid.get_value(G(2, -28));
    std::cerr << "v2 = " << v2 << std::endl;

    float v3 = grid.get_value(G(-42, 28));
    std::cerr << "v3 = " << v3 << std::endl;

    std::cerr << "grid.probe(G(3, 4)) = " << ((int)grid.probe(G(3, 4))) << std::endl;

    std::cerr << "grid.active_cell_count() = " << grid.active_cell_count() << std::endl;

    FTileGrid2f::BoxType box(grid.bbox());
    std::cerr << "box.min_pt() = " << box.min_pt() << std::endl;
    std::cerr << "box.max_pt() = " << box.max_pt() << std::endl;

    for (FTileGrid2f::ValueOnIterator itr = grid.begin_value_on();
         itr.test();
         ++itr) {
      float x = *itr;
      std::cerr << "[" << itr.get_grid_ix() << "],  x = " << x << std::endl;
    }

  }

  std::cerr << "****************************************\n";

  {
    using FixedGrid2_33f = FixedRootGrid2<DenseGrid2<float, 3>, 3>;
    FixedGrid2_33f grid;

    std::cerr << "FixedGrid2_33f::TotalDim = " << FixedGrid2_33f::TotalDim << std::endl;

    mix_t mix = grid.grid_to_offset(G(-21, 4));
    std::cerr << "mix = " << mix << std::endl;
    Vec2Ix gix = grid.offset_to_local_grid(mix);
    std::cerr << "gix = " << gix << std::endl;

    grid.set_value(G(21, 28), 84.);
    grid.set_value(G(2, 18), -20.);
    std::cerr << "grid.get_value(G(21, 28)) = " << grid.get_value(G(21, 28)) << std::endl;
    std::cerr << "grid.get_value(G(41, 2)) = " << grid.get_value(G(41, 2)) << std::endl;
    std::cerr << "grid.get_value(G(2, 18)) = " << grid.get_value(G(2, 18)) << std::endl;

    std::cerr << "grid.active_cell_count() = " << grid.active_cell_count() << std::endl;

    std::cerr << std::endl;

    for (FixedGrid2_33f::ValueOnIterator itr = grid.begin_value_on();
         !itr.done();
         ++itr) {
      std::cerr << "itr.get_grid_ix() = " << itr.get_grid_ix() << std::endl;
      std::cerr << "itr.get_value() = " << itr.get_value() << std::endl;
    }

    std::cerr << std::endl;

    for (FixedGrid2_33f::ChildOnIterator itr = grid.begin_child_on();
         !itr.done();
         ++itr) {
      FixedGrid2_33f::ChildType * child(itr.get_child_ptr());
      std::cerr << "itr.get_offset() = " << itr.get_offset() << std::endl;
      std::cerr << "child->active_cell_count() = " << child->active_cell_count() << std::endl;
    }

  }

  std::cerr << std::endl;

  {
    using FixedGrid2_44f = FixedRootGrid2<DenseGrid2<float, 4>, 4>;
    FixedGrid2_44f grid;
    std::cerr << "FixedGrid2_44f::TotalDim = " << FixedGrid2_44f::TotalDim << std::endl;
  }

  {
    using FixedGrid2_43f = FixedRootGrid2<DenseGrid2<float, 4>, 3>;
    FixedGrid2_43f grid;
    std::cerr << "FixedGrid2_43f::TotalDim = " << FixedGrid2_43f::TotalDim << std::endl;
  }


  return 0;
}
